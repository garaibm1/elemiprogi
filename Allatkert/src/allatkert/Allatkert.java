package allatkert;
public class Allatkert {
    public static void main(String[] args) {
        Kutya k = new Kutya();
        Kutya u = new Kutya("Morzsi", 3, Boolean.TRUE, FajtaEnum.LABRADOR);
       
        
        System.out.println(k.getNev());
        k.setNev("Csibu");
        System.out.println(k.getNev());
        System.out.println(u.getNev());
        System.out.println(u.getKor());
        u.szulinap();
        u.ugat();
        k.ugat();
        System.out.println(u.getKor());
        System.out.println(u.isHim());
        Kutya t = u.parosodik(k);
        System.out.println(t.getNev());
        System.out.println(t.getKor());
        System.out.println(t.isHim());
        Kutya y = Kutya.parosodas(k, u);
        System.out.println(y.getNev());
        System.out.println(y.getKor());
        System.out.println(y.isHim());
        //System.out.println(y.dns);
        //y = Kutya.beoltas(y);
        //System.out.println(y.dns);
        
        Macska m = new Macska("Frisky", 2, MacskaSzin.SZURKE, Macska.OROSZLAN_EN);
        System.out.println("A macskám neve: " + m.getNev());
        System.out.println(m.isVad());
        if(m.isVad()){//m.isVad() == Boolean.TRUE
            m.szelidul();
            System.out.println(m.isVad());
        }
        
        System.out.println(u.getFajta());
        System.out.println(m.getFajta());
        
    }
    
}
